package services;

import java.util.ArrayList;
import java.util.List;

import model.Employee;
import model.Developer;
import model.Manager;

/**
 * 
 * @author ΒΑΣΙΛΙΚΗ
 *
 */
public class EmployeeService {
	
	private List<Employee> employeeList =new ArrayList<>();

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public String toString() {
		return "EmployeeService [employeeList=" + employeeList + "]";
	}

	
	/**
	 * calculate each employee's monthly salary depending on his/her type and bonus
	 * @param empl
	 * @return
	 */
	public double calculateMonthlySalary(Employee empl){
		double totalSalaryEmployee = 10;
		//for managers:
		if(empl instanceof Manager){
			totalSalaryEmployee = empl.getSalary() + (((Manager) empl).getBonus()*(empl.getSalary()))/100;
		}
		//for developers:
		if(empl instanceof Developer){
			totalSalaryEmployee = empl.getSalary();
		}
		return totalSalaryEmployee;
	}
	
	
	/**
	 * calculate the yearly expenses of the company in employee salaries
	 * @param list
	 * @return
	 */
	public double calculateTotalYearlyEmployeeSalaries(List<Employee> list){
		double counter = 0;
		double totalYearlyEmployeeSalaries = 0;
		for(int i=0 ; i<list.size() ; i++){
			counter = this.calculateMonthlySalary(list.get(i));
			totalYearlyEmployeeSalaries += counter;
		}
		System.out.println(totalYearlyEmployeeSalaries);
		return totalYearlyEmployeeSalaries;
	}

	
	/**
	 * hire a new employee
	 * @param employee
	 * @return
	 */
	public boolean hireEmployee(Employee employee){
		boolean status = false;
		employeeList.add(employee);
		System.out.println(employee);
		return status;
	}

	
	/**
	 * fire an employee
	 * @param employee
	 * @return
	 */
	public boolean fireEmployee(Employee employee){
		boolean status = false;
		employeeList.remove(employee);
		System.out.println(employeeList);
		return status;
	}
	
	
	/**
	 * promote a developer of an employee
	 * @param employee
	 * @param bonus
	 */
	public void promoteDeveloperToManager(Employee employee, double bonus){
		if(employee instanceof Developer){
			employee = new Manager(employee.getName(), employee.getSalary(), bonus);
			System.out.println(employee);
		}
	}
}
