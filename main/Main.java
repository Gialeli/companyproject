package main;

import services.EmployeeService;
import model.Company;
import model.Developer;
import model.Employee;
import model.Manager;

public class Main {

	
	public static void main(String[] args) {
		
		EmployeeService employeeService = new EmployeeService();
//		Company company = new Company();
//		Company company1 = new Company("VASD");
		
		System.out.println("============================================");
		
		//test calculateMonthlySalary: #manager
		Employee manager1 = new Manager("Vasiliki", 5655.55 ,2);
		System.out.println(employeeService.calculateMonthlySalary(manager1));
		
		//test calculateMonthlySalary: #developer
		Employee developer1 = new Developer("Anna", 8989);
		System.out.println(employeeService.calculateMonthlySalary(developer1));
		
		
		System.out.println("============================================");
		
		//test fireEmployee:
		Employee employee1 =new Manager("Maria", 777.36, 2);
		Employee employee2 =new Developer("Anthi", 788.36);
		Employee employee3 =new Developer("Giota", 557.36);
		employeeService.hireEmployee(employee1);
		employeeService.hireEmployee(employee2);
		employeeService.hireEmployee(employee3);
		
		System.out.println("============================================");
		
		//test hireEmployee:
		employeeService.fireEmployee(employee2);
		
		System.out.println("============================================");

		//test calculateTotalYearlyEmployeeSalaries:
		employeeService.calculateTotalYearlyEmployeeSalaries(employeeService.getEmployeeList());
		
		System.out.println("============================================");

		//test promote a developer of an employee:
		employeeService.promoteDeveloperToManager(employee3, 2.0);
		
		System.out.println("============================================");
	}

}
