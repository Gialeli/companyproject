package model;

public class Developer extends Employee{
	
	public Developer() {
		super();		
	}
	
	public Developer(String name, double salary) {
		super(name, salary);
	}

	@Override
	public String toString() {
		return "Developer [ Id: " + getId() + ", Name: " + getName()
				+ ", Salary: " + getSalary() + " ]";
	}
}
