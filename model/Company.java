package model;


import java.util.ArrayList;
import java.util.List;

public class Company {

	private String name;
	private  List<Employee> employees;

	public Company() {	}
	
	public Company(String name) {
		super();
		this.name = name;
		employees = new ArrayList<>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public  List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		return "Company [name=" + name + ", employees=" + employees + "]";
	}

}
