package model;

public class Manager extends Employee  {
	
	private double bonus=2;
	
	public Manager() {
		super();		
	}
	
	public Manager(String name, double salary,  double bonus) {
		super(name, salary);
		this.bonus = bonus;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	@Override
	public String toString() {
		return "Manager [ Id: " + getId()
				+ ", Name: " + getName() + ", Salary: " + getSalary()+ ", Bonus: " + bonus +  " ]";
	}

	
}
